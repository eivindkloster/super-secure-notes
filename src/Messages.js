import React, { Component } from 'react';
import $ from "jquery";

import './App.css';
import './Messages.css';

import conf from "./conf.js"

const { token } = conf;

$.ajaxSetup({
    headers: {
      authorization: token
    },
    contentType: "application/json"
})

class Messages extends Component {
    state = {
        message: ""
    }

    hover = false

    componentDidMount() {
        const { channel } = this.props
        //console.log(window.channel_id)

        console.log("MESSAGES MOUNTED")
        console.log(channel)
    }

    sendMessage() {
        console.log(this.state.message)
    }



    render() {
        const { show } = this.props
        console.log(show)
        return (
            <div className="msg-container" style={{visibility: show ? 'unset' : 'hidden'}}>
                <div className="messages-view"></div>
                <div className="send-bar">
                    <input className="message-box" onChange={(e) => {this.setState({message: e.target.value})}}></input>
                    <button className="send-button" onClick={(e) => {this.sendMessage()}}>
                        Send <i className="material-icons arrow back" style={{color: "white", position:"absolute", right: "10px"}}>send</i>
                    </button>
                </div>
            </div>
        )
    }
}

export default Messages;