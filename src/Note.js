import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import SNButton from './Util.js'
import logo from './logo.svg';
import $ from "jquery";

import './App.css';
import './Note.css'

import conf from "./conf.js"

const { token } = conf;

$.ajaxSetup({
    headers: {
      authorization: token
    },
    contentType: "application/json"
})

class Note extends Component {
    constructor(props) {
        super(props)
        this.state = {
            note: {},
            content: "",
            name: "",
            showsave: false,
            saving: false,
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleChange2 = this.handleChange2.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount() {
        // --- !!!!!!!!! ---
        console.log("Note will mount " + this.props.callback)
        console.log("Note will mount ID: " + this.props.noteId)
        this.getData()
    }

    componentWillUnmount() {
        this.props.callback(null)
    }

    getData() {
        var xhr = new XMLHttpRequest()
        xhr.open("GET", `http://localhost:5000/api/documents/${this.props.noteId}`)
        xhr.setRequestHeader("Authorization", token)
        xhr.send()
        
        xhr.onload = (e) => {
            console.log("Responnse note: " + xhr.responseText)
            const note = JSON.parse(xhr.responseText)
            this.setState({
                response: xhr.responseText,
                note: note,
                name: note.title,
                content: note.content
            })
            this.props.callback(note)
        } 
    }

    handleChange(event) {
        // for kompatibilitet
        this.setState({content: event.target.value});

        console.log(this.state)
        this.setState({ text: event.target.value})
        clearTimeout(this.timeout)
        this.timeout = setTimeout(() => this.sendEdits(), 900)

        console.log("input")
    }

    handleChange2(event) {
        this.setState({name: event.target.value});
    }

    sendEdits() {
        console.log("sending edits")
        //console.log(this.state)
        const id  = this.props.noteId
    
        console.log(this.state)

        this.setState({
            showsave: true,
            saving: true
        })
        $.ajax({
          url: `http://localhost:5000/api/documents/${id}`,
          method: "PATCH",
          data: JSON.stringify({content: this.state.content})
        })
            .done(res => {
                this.setState({
                    saving: false
                })
                setTimeout(() => {
                    this.setState({
                        showsave: false
                    })
                }, 750)
            })
    }


    handleSubmit(e) {
        //alert("hi")
        //this.sendEdits()

        

        /*var xhr = new XMLHttpRequest()
        xhr.open("PATCH", `http://localhost:5000/api/documents/${this.props.match.params.id}`)
        xhr.setRequestHeader("Authorization", token)
        xhr.setRequestHeader("Content-Type", "application/json")
        const req = {
            "content": this.state.content//,
            //"name": this.state.name
        }
        this.setState({
            showsave: true,
            saving: true
        })
        xhr.send(JSON.stringify(req))
        xhr.onload = (e) => {
            this.setState({
                saving: false
            })
        }*/
    }

    render() {
        //const note = this.state.note
        const stylerotate = {
            animation: this.state.saving ? 'App-logo-spin infinite 20s linear reverse' : 'none'
        }
        return (
            <div>
                <div className="done" style={{visibility: this.state.showsave ? 'unset' : 'hidden'}}><span><i className="material-icons arrow" style={stylerotate} id="da0">{this.state.saving ? "sync" : "done"}</i></span><p>{this.state.saving ? "Saving" : "Saved!"}</p></div>
                <input className="superrik" value={this.state.name} onChange={this.handleChange2}></input><br />
                <textarea className="big" value={this.state.content} onChange={this.handleChange}/><br />
                {/* <button onClick={this.handleSubmit}>Update!</button><br />
                <SNButton style={{paddingTop: '5px'}}>Save</SNButton> */}
            </div>
        )
    }
}

export default Note