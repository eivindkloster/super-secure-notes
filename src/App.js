import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import logo from './logo.svg';
import './App.css';
import Note from "./Note.js"
import Messages from "./Messages.js"
import conf from "./conf.js"

const { token } = conf;

//const token = "kPLjaZKomNHrsABIXknARAhHwEmSoJmbZGORZDBnAJxcYlee"

var channel_id = "test124"

class App extends Component {

  constructor(props) {
    super(props)
    this.defaultTitle = "Mine notater"
    this.state = {
      response: "",
      notes: [],
      shouldShow: false,
      title: this.defaultTitle,
      showBackbutton: true,
      activeNote: {id: "no"},
      noteActive: false
    }
    this.updateNote = this.updateNote.bind(this)
  }

  componentWillMount() {
    this.getData()
  }

  updateNote(note) {
    console.log("UPDATE NOTE")
    console.log(note)
    if (note === null) {
      console.log("No note")
      this.setState({title: this.defaultTitle, noteActive: false})
      return
    }
    this.setState({activeNote: note, title: note.title, noteActive: true})
  }

  getData() {
    var xhr = new XMLHttpRequest()
    xhr.open("GET", "http://localhost:5000/api/documents")
    xhr.setRequestHeader("Authorization", token)
    xhr.send()
    
    xhr.onload = (e) => {
      console.log("Responnse: " + xhr.responseText)
      localStorage.notes = xhr.responseText

      

      this.setState({
        response: xhr.responseText,
        notes: JSON.parse(xhr.responseText)
      })

    } 
  }

  toggleMessages() {
    if (this.state.shouldShow == true) {
      this.setState({shouldShow: false})
    } else {
      this.setState({shouldShow: true})
    }
    
  }

  listOfNotes() {
    this.state.notes.map((note, index) => {
      console.log("hall")
      console.log(note)
      const access = note.public ? 
      "Public" :
      "Private"
      return (
        <li>
          <p>{note.title} is {access}</p>
        </li>
      )
    })
  }

  render() {
    const backButtonStyle = {
      visibility: this.state.noteActive ? 'unset' : 'hidden'
    }
    return (
        <div className="App">
          <header className="App-header">
            <Link className="linknotugly marx" style={backButtonStyle} to="/"><i className="material-icons arrow back">chevron_left</i></Link>
            <h1 className="App-title"><span><Link className="linknotugly" to="/">{this.state.title}</Link></span></h1>
            <div className="linknotugly right" style={backButtonStyle} onClick={() => this.toggleMessages()}><i className="material-icons arrow back">message</i></div>
          </header>
          <Messages show={this.state.shouldShow} channel={this.state.activeNote.id} key={this.state.activeNote.id}/>
          <Route exact path="/" render={() => (
            <div>
            <p className="App-intro">
              
            </p>
            <ul>
              {this.state.notes.map((note, index) => {
                return (
                  <li key={note.id}>
                    <Link to={`/note/${note.id}`}>{note.title}</Link>
                  </li>
                )
              })}
            </ul>
          </div>
          )} />
          {/*<Route path="/note/:id" component={Note}/>*/}
          <Route path="/note/:id" render={({match}) => (
            <Note noteId={match.params.id} callback={this.updateNote}/>
          )}/>
        </div>
    );
  }
}

export default App;
