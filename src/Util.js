import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import logo from './logo.svg';
import './App.css'
import './Note.css'
import './Util.css'

const SNButton = (props) => {
  return (
    <button className="sn-button">{props.children}</button>
  )
}

export default SNButton